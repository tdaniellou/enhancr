# EnhancR

This project is a small MeteorJS & ReactJS application which purpose is to store expressions and vocabulary of any language.

## Motivation

ReactJS, the JavaScript framework built by Fracebook, has spread crazily across the industry and it will probably be a required skill for front-end developers.

As I was improving my English in an ESL School, I decided to build this tiny application to help me to save, edit and delete vocabulary and expressions that I learned at school. It was the perfect opportunity to learn ReactJS and MeteorJS.

### Tech

EnhancR uses a number of open source projects to work properly:

* [bulma.io] - great UI boilerplate for modern web apps
* [node.js] - evented I/O for the backend

### Installation

EnhancR requires [Node.js] v4+ to run.

* Clone this [repository]
* Install the dependencies and devDependencies and start the server.

```sh
$ cd enhancr
$ npm install -d
$ meteor
```

For production environments...

```sh
TODO
```

### Todos

 - Find a more 'suitable' name
 - Add Code Comments
 - Write Tests

A more detailed todo list is available on [Trello].

## Contributors

* [Thomas Daniellou]

License
----

MIT

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [meteorjs]: <https://www.meteor.com/>
   [repository]: <https://github.com/iNbdy/enhancr.git>
   [thomas daniellou]: <http://daniellou.com>
   [react.js]: <https://facebook.github.io/react/>
   [node.js]: <http://nodejs.org>
   [bulma.io]: <http://bulma.io>
   [trello]: <https://trello.com/b/9neLutgH/enhancr>