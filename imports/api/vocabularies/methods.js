import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { Vocabularies } from './vocabularies.js';

if (Meteor.isServer) {
	Meteor.publish('vocabularies', function vocabulariesPublications() {
		return Vocabularies.find();
	});
}

Meteor.methods({
	'vocabularies.insert'(vocabulary, listId) {
		check(vocabulary, String);
		check(listId, String);

		if (!this.userId) {
			throw new Meteor.Error('not-authorized');
		}

		Vocabularies.insert({
			vocabulary: vocabulary,
			createdAt: new Date(),
			listId: listId,
			owner: this.userId,
			username: Meteor.users.findOne(this.userId).username
		});
	},

	'vocabularies.remove'(vocabularyId) {
		check(vocabularyId, String);

		const vocabulary = Vocabularies.findOne(vocabularyId);

		if (vocabulary.owner !== this.userId) {
			throw new Meteor.Error('not-authorized');
		}

		Vocabularies.remove(vocabularyId);
	},

	'vocabularies.update'(vocabularyId, values) {
		check(vocabularyId, String);
		check(values, Object);

		const vocabulary = Vocabularies.findOne(vocabularyId);

		if (vocabulary.owner !== this.userId) {
			throw new Meteor.Error('not-authorized');
		}

		Vocabularies.update(vocabularyId, { $set: {
			vocabulary: values.vocabulary,
			definition: values.definition,
			synonym: values.synonym
		}});
	}
});