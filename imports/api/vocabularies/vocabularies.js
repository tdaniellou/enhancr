import { Mongo } from 'meteor/mongo';

class VocabulariesCollection extends Mongo.Collection {
	insert(vocabulary, callback) {
		return super.insert(vocabulary, callback);
	}

	remove(selector, callback) {
		return super.remove(selector, callback);
	}
}

export const Vocabularies = new VocabulariesCollection('vocabularies');
