import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { Lists } from './lists.js';

if (Meteor.isServer) {
	Meteor.publish('lists', function listsPublications() {
		return Lists.find();
	});
}

Meteor.methods({
	'lists.insert'(name) {
		check(name, String);

		if (!this.userId) {
			throw new Meteor.Error('not-authorized');
		}

		Lists.insert({
			name: name,
			owner: this.userId,
			public: false
		});
	},

	'lists.update'(listId, values) {
		check(listId, String);
		check(values, Object);

		const list = Lists.findOne(listId);

		if (list.owner !== this.userId) {
			throw new Meteor.Error('not-authorized');
		}

		Lists.update(listId, { $set: {
			name: values.name,
			description: values.description
		}});
	},

	'lists.setPublic'(listId, value) {
		check(listId, String);
		check(value, Boolean);

		Lists.update(listId, { $set: { public: value } });
	},

	'lists.remove'(listId) {
		check(listId, String);

		const list = Lists.findOne(listId);

		if (list.owner !== this.userId) {
			throw new Meteor.Error('not-authorized');
		}

		Lists.remove(listId);
	}
});