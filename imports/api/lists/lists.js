import { Mongo } from 'meteor/mongo';

import { Expressions } from '../expressions/expressions.js';
import { Vocabularies } from '../vocabularies/vocabularies.js';

class ListsCollection extends Mongo.Collection {
	insert(list, callback) {
		let newList = list;

		newList.createdAt = new Date();

		return super.insert(newList, callback);
	}

	remove(selector, callback) {
		// Remove vocabularies
		Vocabularies.remove({ listId: selector });
		// Remove expressions
		Expressions.remove({ listId: selector });

		return super.remove(selector, callback);
	}
}

export const Lists = new ListsCollection('lists');
