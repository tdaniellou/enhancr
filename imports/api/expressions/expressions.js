import { Mongo } from 'meteor/mongo';

class ExpressionsCollection extends Mongo.Collection {
	insert(expression, callback) {
		const newExpression = expression;

		newExpression.createdAt = new Date();

		return super.insert(newExpression, callback);
	}

	delete(selector, callback) {
		return super.delete(selector, callback);
	}
}

export const Expressions = new ExpressionsCollection('expressions');