import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { Expressions } from './expressions.js';

if (Meteor.isServer) {
	Meteor.publish('expressions', function expressionsPublications() {
		return Expressions.find();
	});
}

Meteor.methods({
	'expressions.insert'(expression, listId) {
		check(expression, String);
		check(listId, String);

		if (!this.userId) {
			throw new Meteor.Error('not-authorized');
		}

		Expressions.insert({
			expression: expression,
			listId: listId,
			owner: this.userId,
			username: Meteor.users.findOne(this.userId).username
		});
	},

	'expressions.update'(expressionId, values) {
		check(expressionId, String);
		check(values, Object);

		const expression = Expressions.findOne(expressionId);

		if (expression.owner !== this.userId) {
			throw new Meteor.Error('not-authorized');
		}

		Expressions.update(expressionId, { $set: {
			expression: values.expression,
			definition: values.definition
		}});
	},

	'expressions.remove'(expressionId) {
		check(expressionId, String);

		const expression = Expressions.findOne(expressionId);

		if (expression.owner !== this.userId) {
			throw new Meteor.Error('not-authorized');
		}

		Expressions.remove(expressionId);
	}
});