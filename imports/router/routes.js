import HomePage from '../ui/pages/HomePage';
import LoginPage from '../ui/pages/LoginPage';
import RegisterPage from '../ui/pages/RegisterPage';

import DashboardContainer from '../ui/containers/DashboardContainer';
import ExpressionsContainer from '../ui/containers/ExpressionsContainer';
import VocabularyContainer from '../ui/containers/VocabularyContainer';

import ProfilePage from '../ui/pages/ProfilePage';

import AboutPage from '../ui/pages/AboutPage';
import NotFoundPage from '../ui/pages/NotFoundPage';

import { User } from '../api/User';

function requireLoggedOut(nextState, replace) {
	if (User.isLoggedIn()) {
		replace({
			pathname: '/'
		});
	}
}

function requireLoggedIn(nextState, replace) {
	if (!User.isLoggedIn()) {
		replace({
			pathname: '/login',
			state: { nextPathname: nextState.location.pathname }
		})
	}
}

const routes = [
	{
		path: '/',
		component: HomePage,
	},
	{
		path: '/dashboard',
		component: DashboardContainer,
		onEnter: requireLoggedIn,
	},
	{
		path: '/vocabulary',
		component: VocabularyContainer,
		onEnter: requireLoggedIn
	},
	{
		path: '/expressions',
		component: ExpressionsContainer,
		onEnter: requireLoggedIn
	},
	{
		path: '/about',
		component: AboutPage
	}, {
		path: '/login',
		component: LoginPage,
		onEnter: requireLoggedOut
	}, {
		path: '/register',
		component: RegisterPage,
		onEnter: requireLoggedOut
	}, {
		path: '/profile',
		component: ProfilePage,
		onEnter: requireLoggedIn
	}, {
		path: '*',
		component: NotFoundPage
	}
];

export default routes;
