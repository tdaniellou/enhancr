import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session';

import React from 'react';
import { Router, browserHistory } from 'react-router';
import ReactDOM from 'react-dom';
import { createHistory, useBasename } from 'history';

import routes from '../../router/routes.js';
import AppContainer from '../../ui/containers/AppContainer.jsx';

// Simple method to close the menu when the user changes the route
function closeMenu() {
	Session.set({ menuOpen: false });
}

const rootRoute = {
  component: AppContainer,
  childRoutes: routes,
  onChange: closeMenu
};

Meteor.startup(() => {
  ReactDOM.render(
    <Router history={browserHistory} routes={rootRoute} />,
    document.getElementById('app')
  );
});
