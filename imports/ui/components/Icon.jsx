import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';

export default class Icon extends Component {
	render() {
		const { icon, size } = this.props;
		const iconClass = icon ? 'fa-' + icon : '';
		const sizeClass = size ? 'is-' + size : '';

		const spanClasses = classNames('icon',  sizeClass);
		const iClasses = classNames('fa', iconClass);

		return (
			<span className={ spanClasses }>
				<i className={ iClasses }></i>
			</span>
		);
	}
}

Icon.propTypes = {
	icon: PropTypes.string.isRequired,
	size: PropTypes.string
};