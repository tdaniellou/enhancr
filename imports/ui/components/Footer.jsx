import React, { Component } from 'react';
import { Link } from 'react-router';

class Footer extends Component {
	render() {
		return (
			<footer className="footer">
				<div className="container">
					<div className="content has-text-centered">
						<p>
							<strong>EnhancR</strong>
						</p>
						<p>
							<Link to="/about">About</Link>
						</p>
					</div>
				</div>
			</footer>
		);
	}
}

export default Footer;