import React, { Component } from 'react';
import classNames from 'classnames';
import { Link } from 'react-router';

class LoggedOutLinks extends Component {
	render() {
		const classes = classNames('nav-right',
			{ 'is-active': this.props.toggled
		});

		return (
			<span className="nav-item">
				<Link to="login" className="button is-outlined">
					<span className="icon is-small">
						<i className="fa fa-sign-in"></i>
					</span>
					<span>Sign in</span>
				</Link>
			</span>
		);
	}
}

export default LoggedOutLinks;