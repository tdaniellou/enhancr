import React, { Component } from 'react';
import classNames from 'classnames';

import { User } from '../../../api/User';

class LoggedInLinks extends Component {
	render() {
		const classes = classNames('nav-right',
			{ 'is-active': this.props.toggled
		});

		return (
			<span className="nav-item">
				<a className="button is-outlined" onClick={ this.props.logout }>
					<span className="icon is-small">
						<i className="fa fa-sign-out"></i>
					</span>
					<span>Logout</span>
				</a>
			</span>
		);
	}
}

export default LoggedInLinks;