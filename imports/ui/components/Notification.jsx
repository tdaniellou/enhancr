import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';

class Notification extends Component {
	render() {
		const classes = classNames('notification', this.props.color);

		return (
			<div className={ classes }>
				{ this.props.displayClose ? <button className="delete"></button> : '' }
				{ this.props.content }
			</div>
		);
	}
}

Notification.propTypes = {
	color: PropTypes.string.isRequired,
	displayClose: PropTypes.bool,
	content: PropTypes.string.isRequired
}

export default Notification;