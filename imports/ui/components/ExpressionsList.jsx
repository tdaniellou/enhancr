import React, { Component, PropTypes } from 'react';
import { createContainer } from 'meteor/react-meteor-data';

import { Expressions } from '../../api/expressions/expressions.js';

import Expression from './Expression';

export default class ExpressionsList extends Component {
	renderExpressions() {
		const expressions = this.props.expressions;

		if (expressions.length === 0) {
			return <tr><td colSpan="3">This list is empty</td></tr>
		}

		return expressions.map((expression) => {
			return <Expression key={expression._id} expression={expression} />
		})
	}

	render() {
		return (
			<table className="table is-bordered">
				<thead>
					<tr>
						<th>Expression</th>
						<th>Definition</th>
						<th></th>
						<th></th>
					</tr>
				</thead>

				<tbody>
					{ this.renderExpressions() }
				</tbody>
			</table>
		);
	}
}

ExpressionsList.propTypes = {
	listId: PropTypes.string,
	ready: PropTypes.bool,
	expressions: PropTypes.array
};