import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';

export default class GridRow extends Component {
	render() {
		const { row, colSize } = this.props;
		const columnSize = colSize ? ('is-' + colSize) : '';

		const columnClasses = classNames('column', columnSize);

		return (
			<div className="columns">
				{ row.map((element, i) => {
						return <div className={ columnClasses } key={ i }>{ element }</div>
					})
				}
			</div>
		)
	}
};

GridRow.propTypes = {
	row: PropTypes.array.isRequired,
	colSize: PropTypes.number
}