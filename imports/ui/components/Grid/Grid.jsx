import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';

import GridRow from './GridRow';

export default class Grid extends Component {
	constructor(props) {
		super(props);

		this.renderChildren = this.renderChildren.bind(this);
	}

	renderChildren() {
		const { cols, children } = this.props;
		// TODO: cols must NOT equal to 1
		const size = 12 / (cols - 1);

		// From Bulma.io documentation:
		// Whenever you want to start a new line, you can close a columns container and start a new one.
		// But you can also add the is-multiline modifier and add more column elements that would fit in a single row.
		// TODO: Choose which option is the best or make the user choose it (with the 'multiline' property?).
		return children.reduce((previous, child, i) => {
			// We group the children in groups of 'size'
			if (i % size === 0) {
				// New row
				previous.push([]);
			}

			// Add the child in the latest row
			previous[previous.length - 1].push(child);

			return previous;
		}, []).map((row, i) => {
			// TODO: Size can be either computed or left empty.
			// If it is empty, the column will fit the container automatically.
			return <GridRow key={ i } row={ row } colSize={ size - 1 }/>
		});
	}

	render() {
		return (
			<div>
				{ this.renderChildren() }
			</div>
		);
	}
}

Grid.propTypes = {
	cols: PropTypes.number.isRequired,
	multiline: PropTypes.bool
}
