import React, { Component, PropTypes } from 'react';
import { Link, browserHistory } from 'react-router';
import classNames from 'classnames';

import { User } from '../../../api/User.js';

import Alerts from '../Alert/Alerts';

import LoggedInLinks from '../Auth/LoggedInLinks';
import LoggedOutLinks from '../Auth/LoggedOutLinks';

class Navigation extends Component {
	constructor(props) {
		super(props);

		this.handleClick = this.handleClick.bind(this);
	}

	handleClick() {
		this.props.onToggle();
	}

	render() {
		const authLinks = User.isLoggedIn() ? <LoggedInLinks logout={ this.props.logout } /> : <LoggedOutLinks />;

		return (
			<nav className="nav custom-nav is-black">
				<div className="nav-left">
					<span className="hamburger" onClick={ this.handleClick }>
						<span></span>
						<span></span>
						<span></span>
					</span>
				</div>

				<div className="nav-center">
					<Link to="/" className="nav-item hero-brand touchable">EnhancR</Link>
				</div>

				<div className="nav-right">
					{ authLinks }
				</div>
			</nav>
		);
	}
}

Navigation.propTypes = {
	onToggle: PropTypes.func.isRequired
};

export default Navigation;