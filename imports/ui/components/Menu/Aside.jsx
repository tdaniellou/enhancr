import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';

import MenuLink from './MenuLink';

class Aside extends Component {
	render() {
		const { user } = this.props;

		return (
			<div id="sidebar">
				<aside className="menu">
					<p className="menu-label">
						General
					</p>

					{ !_.isEmpty(user) ?
						<div>
							<ul className="menu-list">
								<li><MenuLink to="/">Home</MenuLink></li>
								<li><MenuLink to="/dashboard">Dashboard</MenuLink></li>
								<li><MenuLink to="/vocabulary">Vocabulary</MenuLink></li>
								<li><MenuLink to="/expressions">Expressions</MenuLink></li>
							</ul>

							<p className="menu-label">
								Users
							</p>

							<ul className="menu-list">
								<li>
									<MenuLink to="/profile">Profile</MenuLink>
									<ul>
										<li><MenuLink to="/profile">Your lists</MenuLink></li>
										<li><MenuLink to="/profile">Starred lists</MenuLink></li>
									</ul>
								</li>
								<li>
									<a>Logout</a>
								</li>
							</ul>
						</div>
						:
						<ul className="menu-list">
							<li><MenuLink to="/">Home</MenuLink></li>
							<li><MenuLink to="/login">Login</MenuLink></li>
							<li><MenuLink to="/register">Register</MenuLink></li>
						</ul>
					}

					{ !_.isEmpty(user) && user.isAdmin ?
						<div>
							<p className="menu-label">
								Administration
							</p>

							<ul className="menu-list">
								<li>
									<a href="#">Manage Content</a>
									<ul>
										<li><a href="#">Members</a></li>
										<li><a href="#">Lists</a></li>
										<li><a href="#">Vocabulary</a></li>
										<li><a href="#">Expressions</a></li>
									</ul>
								</li>
							</ul>
						</div> : ''
					}

				</aside>
			</div>
		);
	}
}

Aside.propTypes = {
	user: PropTypes.object
};

export default Aside;