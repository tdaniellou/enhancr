import React, { Component } from 'react';

import { Link } from 'react-router';

class MenuLink extends Component {
	render() {
		return (
			<Link activeClassName="is-active" { ...this.props }/>
		);
	}
}

export default MenuLink;