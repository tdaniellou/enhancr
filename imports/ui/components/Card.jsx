import React, { Component, PropTypes } from 'react';

class Card extends Component {
	render() {
		const { title, subtitle, description, date } = this.props;

		return (
			<div className="card is-fullwidth">
				<div className="card-image">
					<figure className="image is-4by3">
						<img src="http://placehold.it/300x225" alt="" />
					</figure>
				</div>
				<div className="card-content">
					<div className="media">
						<div className="media-left">
						<figure className="image is-32x32">
							<img src="http://placehold.it/64x64" alt="Image" />
						</figure>
						</div>
						<div className="media-content">
						<p className="title is-5">{ title }</p>
						<p className="subtitle is-6">{ subtitle }</p>
						</div>
					</div>

					<div className="content">
						{ description }
						<br />
						<small>11:09 PM - 1 Jan 2016</small>
					</div>
				</div>
			</div>
		);
	}
}

Card.propTypes = {
	title: PropTypes.string.isRequired,
	subtitle: PropTypes.string,
	description: PropTypes.string,
	date: PropTypes.object
};

export default Card;