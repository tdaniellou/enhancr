import React, { Component, PropTypes } from 'react';

import Vocabulary from './Vocabulary';

class VocabularyList extends Component {
	renderVocabulary() {
		const vocabularies = this.props.vocabularies;

		if (vocabularies.length === 0) {
			return <tr><td colSpan="4">This list is empty</td></tr>
		}

		return vocabularies.map((vocabulary) => {
			return <Vocabulary key={ vocabulary._id } vocabulary={ vocabulary }/>;
		});
	}

	render() {
		return (
			<table className="table is-bordered">
				<thead>
					<tr>
						<th>Vocabulary</th>
						<th>Definition</th>
						<th>Synonym</th>
						<th>Actions</th>
					</tr>
				</thead>

				<tbody>
					{ this.props.ready ? this.renderVocabulary() : '' }
				</tbody>
			</table>
		)
	}
}

VocabularyList.propTypes = {
	listId: PropTypes.string,
	ready: PropTypes.bool,
	vocabularies: PropTypes.array
}

export default VocabularyList;