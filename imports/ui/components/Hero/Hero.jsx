import React, { Component } from 'react';
import classNames from 'classnames';

class Hero extends Component {
	render() {
		const { size, color } = this.props;

		const heroClasses = classNames('hero', color, {
			'is-medium': size === 'medium',
			'is-large': size === 'large',
			'is-fullheight': size === 'fullheight'
		});

		return (
			<section className={ heroClasses }>
				{ this.props.children }
			</section>
		);
	}
}

export default Hero;