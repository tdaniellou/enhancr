import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';

export default class HeroBody extends Component {
	render() {
		const { align } = this.props;

		const classes = classNames('container', {
			'has-text-centered': align === 'center',
			'has-text-left': align === 'left',
			'has-text-right': align === 'right'
		});

		return (
			<div className="hero-body">
				<div className={ classes }>
					{ this.props.children }
				</div>
			</div>
		);
	}
}

HeroBody.propTypes = {
	align: PropTypes.string
};