import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';

export default class HeroFooter extends Component {
	render() {
		const { align } = this.props;

		const classes = classNames('container', {
			'has-text-centered': align === 'center',
			'has-text-left': align === 'left',
			'has-text-right': align === 'right'
		});

		return (
			<div className="hero-foot">
				<div className={ classes }>
					{ this.props.children }
				</div>
			</div>
		);
	}
}

HeroFooter.propTypes = {
	align: PropTypes.string
};