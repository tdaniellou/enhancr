import React, { Component } from 'react';
import classNames from 'classnames';

class HeaderToggler extends Component {
	constructor(props) {
		super(props);

		this.onClick = this.onClick.bind(this);
	}

	onClick() {
		this.props.onToggle();
	}

	render() {
		var classes = classNames('nav-toggle', {
			'is-active': this.props.toggled
		});

		return (
			<span onClick={ this.onClick } className={ classes }>
				<span></span>
				<span></span>
				<span></span>
			</span>
		);
	}
}

export default HeaderToggler;