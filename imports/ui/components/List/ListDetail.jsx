import React, { Component, PropTypes } from 'react';
import { createContainer } from 'meteor/react-meteor-data';

import { Vocabularies } from '../../../api/vocabularies/vocabularies.js';
import { Expressions } from '../../../api/expressions/expressions.js';

import FormattedDate from '../FormattedDate';

import Alerts from '../Alert/Alerts';
import EditableText from '../Form/EditableText';

class ListDetail extends Component {
	constructor(props) {
		super(props);

		this.deleteList = this.deleteList.bind(this);
		this.updateName = this.updateName.bind(this);
		this.updateDescription = this.updateDescription.bind(this);
	}

	deleteList(event) {
		this.props.onDelete(this.props.list._id);
	}

	updateName(name, callback) {
		let newValues = this.props.list;

		newValues.name = name;

		this.props.onUpdate(newValues, callback);
	}

	updateDescription(description, callback) {
		let newValues = this.props.list;

		newValues.description = description;

		this.props.onUpdate(newValues, callback);
	}

	render() {
		const { list, vocabularyCount, expressionCount } = this.props;

		return (
			<div className="box">
	  			<div className="content">
					<div className="columns">
						<div className="column is-9">
							<div className="title is-4"><EditableText type="text" label="name" text={ list.name } onEdit={ this.updateName }/></div>
							<div className="subtitle is-6"><FormattedDate date={ list.createdAt } /></div>

							<div className="control">
								<EditableText type="textarea" label="description" text={ list.description } onEdit={ this.updateDescription } allowEmpty={ true }/>
							</div>
						</div>

						<div className="column">
							<div className="is-pulled-right">
								<p className="control has-addons">
									<a className="button is-small is-info">
										<span className="icon is-small">
											<i className="fa fa-cog"></i>
										</span>
										<span>Settings</span>
									</a>
									<a className="button is-small is-danger" onClick={ this.deleteList }>
										<span className="icon is-small">
											<i className="fa fa-trash"></i>
										</span>
										<span>Delete</span>
									</a>
								</p>
								<p className="content has-text-right">
									<strong>{ list.public ? 'Public' : 'Private'} List</strong>
									<br />
									Vocabulary count: <strong>{ vocabularyCount }</strong>
									<br />
									Expressions count: <strong>{ expressionCount }</strong>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

ListDetail.propTypes = {
	list: PropTypes.object.isRequired,
	vocabularyCount: PropTypes.number.isRequired,
	expressionCount: PropTypes.number.isRequired,
	onUpdate: PropTypes.func,
	onDelete: PropTypes.func
};

export default createContainer((params) => {
	const { list, onDelete } = params;
	const listId = list._id;

	const vocabulariesHandle = Meteor.subscribe('vocabularies');
	const expressionsHandle = Meteor.subscribe('expressions');

	const vocabularies = Vocabularies.find({ listId: listId });
	const expressions = Expressions.find({ listId: listId });

	const vocabulariesReady = vocabulariesHandle.ready();
	const expressionsReady = expressionsHandle.ready();

	const vocabularyCount = vocabulariesReady ? vocabularies.count() : 0;
	const expressionCount = expressionsReady ? expressions.count() : 0;

	return {
		list: list,
		onDelete: onDelete,
		vocabularyCount: vocabularyCount,
		expressionCount: expressionCount
	};
}, ListDetail);