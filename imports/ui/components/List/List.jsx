import React, { Component, PropTypes } from 'react';

import Alerts from '../Alert/Alerts';

class List extends Component {
	constructor(props) {
		super(props);

		this.togglePrivate = this.togglePrivate.bind(this);
	}

	togglePrivate(event) {
		event.preventDefault();

		const action = !this.props.list.public;

		Meteor.call('lists.setPublic', this.props.list._id, action, (error) => {
			if (error) {
				Alerts.error(error.reason);
			} else {
				const info = action ? 'public' : 'private';

				Alerts.success('The list is now ' + info, 'Undo');
			}
		});
	}

	render() {
		const icon = this.props.list.public ? <i className="fa fa-unlock"></i> : <i className="fa fa-lock"></i>;

		return (
			<li>
				{ this.props.list.name } (<a onClick={ this.props.onClick }>Quick view</a>)

				<a className="icon" onClick={ this.togglePrivate }>
					{ icon }
				</a>
			</li>
		);
	}
}

List.propTypes = {
	list: PropTypes.object.isRequired
}

export default List;