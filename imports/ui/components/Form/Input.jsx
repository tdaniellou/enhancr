import React, { Component, PropTypes } from 'react';

import classNames from 'classnames';

class Input extends Component {
	constructor(props) {
		super(props);

		// Store the value in the state to make the input 'reactive'
		this.state = {
			value: this.props.value
		};

		this.getValue = this.getValue.bind(this);
		this.setValue = this.setValue.bind(this);
		this.clear = this.clear.bind(this);

		this.onChange = this.onChange.bind(this);

		this.generateInput = this.generateInput.bind(this);
	}

	componentDidMount() {
		if (this.props.autofocus) {
			this.refs.input.focus();
		}
	}

	getValue() {
		const { value } = this.state;

		return value ? value.trim() : '';
	}

	setValue(value) {
		this.setState({ value: value });
	}

	clear() {
		this.setValue('');
	}

	onChange(event) {
		this.setState({ value: event.target.value });
	}

	generateInput() {
		const { type, label, size, onBlur } = this.props;
		let { value } = this.state
		let inputClasses = classNames(type, {
			'textarea': type === 'textarea',
			'input': type !== 'textarea',
			'is-small': size === 'small',
			'is-medium': size === 'medium',
			'is-large': size === 'large'
		});

		// Empty string if the value is undefined
		value = value ? value : '';

		if (type === 'textarea') {
			return <textarea className={ inputClasses } ref="input" value={ value } onChange={ this.onChange } onBlur={ onBlur }/>;
		}

		return <input className={ inputClasses }
			ref="input"
			type={ type }
			placeholder={ label }
			value={ value }
			onChange={ this.onChange }
			onBlur={ onBlur }/>
	}

	render() {
		let classes = classNames('control', 'is-expanded', {
			'has-icon': this.props.icon,
		});

		return (
			<p className={ classes }>
				{ this.generateInput() }
				{ this.props.icon ? <i className={ this.props.icon }></i> : '' }
			</p>
		);
	}
}

Input.propTypes = {
	type: PropTypes.string.isRequired,
	label: PropTypes.string,
	icon: PropTypes.string,
	value: PropTypes.string,
	autofocus: PropTypes.bool,
	onBlur: PropTypes.func
};

export default Input;