import React, { Component, PropTypes } from 'react';

import Input from './Input';

class EditableText extends Component {
	constructor(props) {
		super(props);

		this.state = {
			editing: false,
			text: this.props.text
		};

		this.toggleEditing = this.toggleEditing.bind(this);
		this.generateValue = this.generateValue.bind(this);

		this.updateValue = this.updateValue.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	/**
	 * Toggles the editing state.
	 */
	toggleEditing() {
		this.setState({ editing: !this.state.editing });
	}

	/**
	 * Checks the user's new value, fires the callback property if it is defined, and exit the edit mode.
	 */
	updateValue() {
		const { allowEmpty, onEdit } = this.props;

		if (onEdit) {
			const value = this.refs.input.getValue();

			if (!allowEmpty && value.length === 0) {
				this.toggleEditing();

				return;
			}

			// Update if the value is different
			if (value !== this.state.text) {
				// Fire the callback
				onEdit(value, () => { this.setState({ text: value }); });
			}
		}

		this.toggleEditing();
	}

	/**
	 * Callback fired when the form is submitted.
	 * This is used to catch when the user hits the 'Enter' key.
	 *
	 * @param  Object event the event
	 */
	handleSubmit(event) {
		event.preventDefault();

		this.updateValue();
	}

	/**
	 * Generates the value to display from the component's properties.
	 * If the 'text' property is empty, it will display 'Click to edit' + label.
	 * Otherwise, it will display a clickable link with the value of the text.
	 */
	generateValue() {
		const { text } = this.state;
		const { label } = this.props;
		let value;

		if (text && text.length > 0) {
			value = text;
		} else {
			value = (
				<span>
					<span className="icon is-small"><i className="fa fa-pencil"></i></span> Click to edit { label }
				</span>
			);
		}

		return (
			<a onClick={ this.toggleEditing }>
				{ value }
			</a>
		);
	}

	render() {
		if (this.state.editing) {
			return (
				<form onSubmit={ this.handleSubmit }>
					<Input ref="input" type={ this.props.type } value={ this.state.text } autofocus={ true } onBlur={ this.updateValue }/>
				</form>
			);
		}

		return this.generateValue();
	}
}

EditableText.propTypes = {
	/**
	 * The type of input to be displayed on edit mode
	 * @type string, isRequired
	 */
	type: PropTypes.string.isRequired,

	/**
	 * The label of the component
	 * @type string
	 */
	label: PropTypes.string,

	/**
	 * The value of the component
	 * @type string
	 */
	text: PropTypes.string,

	/**
	 * Callback fired when the edit is finished.
	 * The callback must have 2 arguments:
	 * 	- The first one will have the new value
	 * 	- The second one is the callback to fire when the value is consumed
	 * IMPORTANT: The second argument MUST be fired in order to be updated here
	 * @type function
	 */
	onEdit: PropTypes.func,

	/**
	 * Whether the value can be empty or not.
	 * @type boolean
	 */
	allowEmpty: PropTypes.bool
};

export default EditableText;