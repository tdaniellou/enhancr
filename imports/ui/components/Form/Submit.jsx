import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';

class Submit extends Component {
	render() {
		const classes = classNames('button',  'inline-block',
			this.props.color ? this.props.color : 'is-primary',
			this.props.size ? this.props.size : '');

		const submit = this.props.onSubmit
			? <button className={ classes } onClick={ this.props.onSubmit }>{ this.props.label }</button>
			: <button className={ classes }>{ this.props.label }</button>

		return (
			<p className="control" { ...this.props }>
				{ submit }
			</p>
		);
	}
}

Submit.propTypes = {
	label: PropTypes.string.isRequired,
	size: PropTypes.string,
	onSubmit: PropTypes.func
};

export default Submit;