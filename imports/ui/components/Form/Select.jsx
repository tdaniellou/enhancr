import React, { Component, PropTypes } from 'react';

class Select extends Component {
	constructor(props) {
		super(props);

		this.onChange = this.onChange.bind(this);
		this.renderOptions = this.renderOptions.bind(this);
	}

	renderOptions() {
		return this.props.options.map((option) => {
			return <option key={ option.id } value={ option.id }>{ option.name }</option>;
		});
	}

	onChange(event) {
		this.props.onChange(event.target.value);
	}

	render() {
		return (
			<div className="control is-horizontal">
				<p className="control">
					<span className="select is-fullwidth">
						<select onChange={ this.onChange }>
							{ this.renderOptions() };
						</select>
					</span>
				</p>
			</div>
		);
	}
}

Select.proTypes = {
	options: PropTypes.array.isRequired,
	onChange: PropTypes.func
};

export default Select;