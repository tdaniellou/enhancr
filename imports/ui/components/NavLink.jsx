import React, { Component } from 'react';

import { Link } from 'react-router';

class NavLink extends Component {
	render() {
		return (
			<Link className="nav-item" activeClassName="is-active" { ...this.props }/>
		);
	}
}

export default NavLink;