import React, { Component, PropTypes } from 'react';

class FormattedDate extends Component {
	render() {
		const { date } = this.props;
		let { format } = this.props;

		// Default format
		format = format ? format : "MMM DD, YYYY h:mm A";

		return <span>{ moment(date).format(format) }</span>;
	}
}

FormattedDate.proTypes = {
	date: PropTypes.string.isRequired,
	format: PropTypes.string
};

export default FormattedDate;