import React, { Component, PropTypes } from 'react';

import { Vocabularies } from '../../api/vocabularies/vocabularies.js';

import Alerts from './Alert/Alerts';

import EditableText from './Form/EditableText';

class Vocabulary extends Component {
	constructor(props) {
		super(props);

		this.updateVocabulary = this.updateVocabulary.bind(this);
		this.updateDefinition = this.updateDefinition.bind(this);
		this.updateSynonym = this.updateSynonym.bind(this);

		this.updateEntry = this.updateEntry.bind(this);
		this.deleteEntry = this.deleteEntry.bind(this);
	}

	updateEntry(newValues, callback) {
		const { _id } = newValues;

		Meteor.call('vocabularies.update', _id, newValues, (error) => {
			if (error) {
				Alerts.danger(error.reason);
			} else {
				Alerts.success('Vocabulary updated', 'Undo');

				// Fire callback
				if (callback) {
					callback();
				}
			}
		});
	}

	updateVocabulary(vocabulary, callback) {
		let newValues = this.props.vocabulary;

		newValues.vocabulary = vocabulary;

		this.updateEntry(newValues, callback);
	}

	updateDefinition(definition, callback) {
		let newValues = this.props.vocabulary;

		newValues.definition = definition;

		this.updateEntry(newValues, callback);
	}

	updateSynonym(synonym, callback) {
		let newValues = this.props.vocabulary;

		newValues.synonym = synonym;

		this.updateEntry(newValues, callback);
	}

	deleteEntry() {
		Meteor.call('vocabularies.remove', this.props.vocabulary._id, (error) => {
			if (error) {
				Alerts.error(error.reason);
			} else {
				Alerts.success('Vocabulary deleted', 'Undo');
			}
		});
	}

	render() {
		const { vocabulary, definition, synonym } = this.props.vocabulary;

		return (
			<tr>
				<td><EditableText type="text" text={ vocabulary } onEdit={ this.updateVocabulary }/></td>
				<td><EditableText type="text" text={ definition } onEdit={ this.updateDefinition } allowEmpty={ true }/></td>
				<td><EditableText type="text" text={ synonym } onEdit={ this.updateSynonym } allowEmpty={ true }/></td>
				<td className="is-icon">
					<a href="#">
						<i className="fa fa-archive"></i>
					</a>
					<a onClick={ this.deleteEntry }>
						<i className="fa fa-trash"></i>
					</a>
				</td>
			</tr>
		);
	}
}

Vocabulary.propTypes = {
	vocabulary: PropTypes.object.isRequired
};

export default Vocabulary;