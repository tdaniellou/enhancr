import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import Alert from './Alert';

const containerId = 'alert-container';
const defaultTimeout = 5000;

// TODO: Implement animation
const animationTime = 0;
const delayBetweenAnimation = 100;

// Timer handle to keep track of alerts being rendered
let timer = 0;

class Alerts extends Component {
	/**
	 * Renders an alert inside the container
	 *
	 * @param  {[type]}   message  [description]
	 * @param  {[type]}   type     [description]
	 * @param  {[type]}   action   [description]
	 * @param  {Function} callback [description]
	 */
	static renderAlert(message, type, action, callback) {
		const alert = <Alert message={ message } type={ type } action={ action } callback={ callback }/>;

		ReactDOM.render(alert, document.getElementById(containerId));
	}

	static info(message, action, callback) {
		this.create(message, 'is-info', action, callback);
	}

	static success(message, action, callback) {
		this.create(message, 'is-success', action, callback);
	}

	static warning(message, action, callback) {
		this.create(message, 'is-warning', action, callback);
	}

	static error(message, action, callback) {
		this.create(message, 'is-danger', action, callback);
	}

	/**
	 * Creates an alert, renders it and hides it after a certain time.
	 *
	 * @param  {[type]}   message  [description]
	 * @param  {[type]}   type     [description]
	 * @param  {[type]}   action   [description]
	 * @param  {Function} callback [description]
	 */
	static create(message, type, action, callback) {
		// If an alert is already being rendered, we hide it
		if (timer !== 0) {
			this.hideAlert();
		}

		// Display the alert
		setTimeout(() => {
			this.renderAlert(message, type, action, callback);
		}, animationTime + delayBetweenAnimation);

		// Create a timeout that will hide the current alert.
		timer = setTimeout(this.hideAlert, defaultTimeout);
	}

	/**
	 * Hides the current alert and reset the timer handle.
	 *
	 * @return {[type]} [description]
	 */
	static hideAlert() {
		ReactDOM.unmountComponentAtNode(document.getElementById(containerId));

		// Reset the timer handle
		clearTimeout(timer);
		timer = 0;
	}

	render() {
		return (
			<div id={ containerId }></div>
		);
	}
}

export default Alerts;