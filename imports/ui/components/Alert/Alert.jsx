import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';

class Alert extends Component {
	constructor(props) {
		super(props);

		this.handleActionClick = this.handleActionClick.bind(this);
	}

	handleActionClick(event) {
		event.preventDefault();

		if (this.props.callback) {
			this.props.callback();
		}
	}

	render() {
		const notificationClasses = classNames('alert', this.props.type);

		return (
			<div className={ notificationClasses }>
				{ this.props.message }
				{ this.props.action ? <a className="action" onClick={ this.handleActionClick }>{ this.props.action }</a> : '' }
			</div>
		);
	}
}

Alert.propTypes = {
	/**
	 * The message to be displayed
	 * @type string
	 */
	message: PropTypes.string.isRequired,

	/**
	 * The type of the message
	 * @type string
	 */
	type: PropTypes.string.isRequired,

	/**
	 * The action message
	 * @type string
	 */
	action: PropTypes.string,

	/**
	 * The action callback
	 * @type func
	 */
	callback: PropTypes.func
}

export default Alert;