import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';

class Modal extends Component {
	render() {
		const modalClasses = classNames('modal', {
			'is-active': this.props.isOpen
		});

		return (
			<div className={ modalClasses }>
				<div className="modal-background" onClick={ this.props.onClose }></div>
				<div className="modal-container">
					<div className="modal-content">
						{ this.props.children }
					</div>
				</div>
				<button className="modal-close" onClick={ this.props.onClose }></button>
			</div>
		);
	}
}

Modal.propTypes = {
	isOpen: PropTypes.bool.isRequired,
	onClose: PropTypes.func.isRequired
};

export default Modal;