import React, { Component } from 'react';

import { Expressions } from '../../api/expressions/expressions.js';

import Alerts from './Alert/Alerts';

import EditableText from './Form/EditableText';

class Expression extends Component {
	constructor(props) {
		super(props);

		this.state = {
			editing: false
		};

		this.updateEntry = this.updateEntry.bind(this);
		this.updateExpression = this.updateExpression.bind(this);
		this.updateDefinition = this.updateDefinition.bind(this);

		this.deleteEntry = this.deleteEntry.bind(this);
	}

	updateEntry(newValues, callback) {
		const { _id } = newValues;

		// Update the expression
		Meteor.call('expressions.update', _id, newValues, (error) => {
			if (error) {
				// Display error
				Alerts.error(error.reason);
			} else {
				Alerts.success('Expression updated', 'Undo');

				if (callback) {
					callback();
				}
			}
		});
	}

	updateExpression(expression, callback) {
		const newValues = this.props.expression;

		newValues.expression = expression;

		this.updateEntry(newValues, callback);
	}

	updateDefinition(definition, callback) {
		const newValues = this.props.expression;

		newValues.definition = definition;

		this.updateEntry(newValues, callback);
	}

	deleteEntry() {
		const { _id } = this.props.expression;

		Meteor.call('expressions.remove', _id, (error) => {
			if (error) {
				Alerts.error(error.reason);
			} else {
				Alerts.success('Expression deleted', 'Undo');
			}
		});
	}

	render() {
		const { expression, definition } = this.props.expression;

		return (
			<tr>
				<td><EditableText type="text" text={ expression } onEdit={ this.updateExpression }/></td>
				<td><EditableText type="text" text={ definition } onEdit={ this.updateDefinition } allowEmpty={ true }/></td>
				<td className="is-icon">
					<a href="#">
						<i className="fa fa-archive"></i>
					</a>
				</td>
				<td className="is-icon">
					<a onClick={ this.deleteEntry }>
						<i className="fa fa-trash"></i>
					</a>
				</td>
			</tr>
		);
	}
}

export default Expression;