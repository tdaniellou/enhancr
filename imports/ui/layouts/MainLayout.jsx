import React, { Component, PropTypes } from 'react';
import { Session } from 'meteor/session';
import { browserHistory } from 'react-router';
import classNames from 'classnames';

import Navigation from '../components/Menu/Navigation';
import Aside from '../components/Menu/Aside';
import Footer from '../components/Footer';

import Alerts from '../components/Alert/Alerts';

class MainLayout extends Component {
	constructor(props) {
		super(props);

		this.logout = this.logout.bind(this);
		this.toggleMenu = this.toggleMenu.bind(this);
		this.closeMenu = this.closeMenu.bind(this);
	}

	logout() {
		Meteor.logout((error) => {
			if (error) {
				console.log(error);
			} else {
				Alerts.info('You are now logged out');

				// Redirect to Home
				browserHistory.push('/');
			}
		});
	}

	toggleMenu(menuOpen = !Session.get('menuOpen')) {
		Session.set({ menuOpen: menuOpen });
	}

	closeMenu() {
		this.toggleMenu(false);
	}

	render() {
		const { user, menuOpen } = this.props;

		const containerClasses = classNames({
			'menu-open': menuOpen
		});

		return (
		 	<div id="container" className={ containerClasses }>
				<Alerts />
				<Aside user={ user } logout={ this.logout }/>

				<div className="content-overlay" onClick={ this.closeMenu }></div>

				<div id="content-container">
		 			<Navigation user={ user } logout={ this.logout } onToggle={ this.toggleMenu }/>

					<main id="content">
						{ this.props.children }
					</main>

					<Footer />
				</div>
		  	</div>
		);
	}
}

MainLayout.propTypes = {
	user: PropTypes.object,
	menuOpen: PropTypes.bool
};

export default MainLayout;