import { createContainer } from 'meteor/react-meteor-data';
import { Session } from 'meteor/session';

import { User } from '../../api/User';

import MainLayout from '../layouts/MainLayout';

export default createContainer(() => {
	return {
		user: User.get(),
		menuOpen: Session.get('menuOpen')
	};
}, MainLayout);