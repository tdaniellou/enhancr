import { createContainer } from 'meteor/react-meteor-data';

import { Expressions } from '../../api/expressions/expressions.js';

import ExpressionsList from '../components/ExpressionsList';

export default createContainer((params) => {
	const listId = params.listId;
	const expressionsHandle = Meteor.subscribe('expressions');
	const ready = expressionsHandle.ready();
	const expressions = Expressions.find({ listId: listId }, { sort: { createdAt: -1 } });
	const expressionsExist = ready && !!expressions;

	return {
		listId: listId,
		ready: ready,
		expressions: expressionsExist ? expressions.fetch() : []
	};
}, ExpressionsList);