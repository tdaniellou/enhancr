import { createContainer } from 'meteor/react-meteor-data';

import { Vocabularies } from '../../api/vocabularies/vocabularies.js';

import VocabularyList from '../components/VocabularyList';

export default createContainer((params) => {
	const listId = params.listId;
	const vocabulariesHandle = Meteor.subscribe('vocabularies');
	const ready = vocabulariesHandle.ready();
	const vocabularies = Vocabularies.find({ listId: listId }, { sort: { createdAt: -1 } });
	const vocabulariesExists = ready && !!vocabularies;

	return {
		listId: listId,
		ready: ready,
		vocabularies: vocabulariesExists ? vocabularies.fetch() : []
	};
}, VocabularyList);