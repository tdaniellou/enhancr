import { createContainer } from 'meteor/react-meteor-data';

import { User } from '../../api/User.js';
import { Lists } from '../../api/lists/lists.js';

import VocabularyPage from '../pages/VocabularyPage';

export default createContainer(() => {
	Meteor.subscribe('lists');

	return {
		lists: Lists.find({ owner: User.id() }, { sort: { createdAt: -1 } }).fetch(),
		currentUser: User.get()
	};
}, VocabularyPage);