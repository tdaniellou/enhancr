import { createContainer } from 'meteor/react-meteor-data';

import { User } from '../../api/User.js';
import { Lists } from '../../api/lists/lists.js';

import DashboardPage from '../pages/DashboardPage';

export default createContainer(() => {
	Meteor.subscribe('lists');

	return {
		lists: Lists.find({ owner: User.id() }, { sort: { createdAt: -1 } }).fetch(),	// The user's lists
		currentUser: User.get(),														// The user object, to check if the user is logged in
	};
}, DashboardPage);