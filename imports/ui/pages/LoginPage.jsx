import React, { Component } from 'react';
import { Link, browserHistory } from 'react-router';

import Notification from '../components/Notification';

import Alerts from '../components/Alert/Alerts';

import Input from '../components/Form/Input';
import Submit from '../components/Form/Submit';

class LoginPage extends Component {
	constructor(props) {
		super(props);

		this.state = {
			errors: {}
		};

		this.onSubmit = this.onSubmit.bind(this);
	}

	onSubmit(event) {
		event.preventDefault();

		// Reset the errors in the state
		this.setState({ errors: {} });

		// Get the form input DOMNodes
		const email = this.refs.email;
		const password = this.refs.password;

		const emailValue = email.getValue();
		const passwordValue = password.getValue();

		// The list of errors
		const errors = {};

		if (!emailValue) {
			errors.email = 'Username or email is required';
		}

		if (!passwordValue) {
			errors.password = 'Password is required';
		}

		if (!_.isEmpty(errors)) {
			this.setState({ errors: errors });

			return;
		}

		Meteor.loginWithPassword(emailValue, passwordValue,
			(error) => {
				if (error) {
					Alerts.error(error.reason);
				} else {
					// Clear the form
					email.clear();
					password.clear();

					Alerts.info('Welcome!');

					// Redirect
					const { location } = this.props;

					if (location.state && location.state.nextPathname) {
						browserHistory.push(location.state.nextPathname);
					} else {
						browserHistory.push('/');
					}
				}
			}
		);
	}

	render() {
		const formErrors = Object.keys(this.state.errors).map((key) => {
			return <Notification key={ key } content={ this.state.errors[key] } color="is-danger"/>;
		});

		return (
			<section className="section">
				<div className="container">
					<div className="columns">
						<div className="column is-one-third is-offset-one-third">
							<form onSubmit={ this.onSubmit } className="auth-form">
								<h1 className="title">Login</h1>
								{ formErrors }
								<div>
									<Input ref="email" type="text" label="Username or email" icon="fa fa-user" autofocus={ true }/>
									<Input ref="password" type="password" label="Password" icon="fa fa-lock" />
									<Submit label="Submit" />
								</div>
							</form>
							<p className="auth-alt">Don't have an account? <Link to="/register">Register</Link></p>
						</div>
					</div>
				</div>
			</section>
		);
	}
}

export default LoginPage;