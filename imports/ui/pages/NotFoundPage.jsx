import React, { Component } from 'react';
import { Link } from 'react-router';


class NotFoundPage extends Component {
	render() {
		return (
			<section className="section">
				<div className="container is-fluid">
					<div className="content has-text-centered">
						<h1>This page can not be found</h1>
						<Link to="/" className="button is-primary is-inverted">&larr; Return</Link>
					</div>
				</div>
			</section>
		);
	}
}

export default NotFoundPage;
