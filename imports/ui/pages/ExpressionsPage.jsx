import React, { Component, PropTypes } from 'react';

import ExpressionsList from '../containers/ExpressionsListContainer';

import Alerts from '../components/Alert/Alerts';

import Input from '../components/Form/Input';
import Select from '../components/Form/Select';
import Submit from '../components/Form/Submit';

// TODO: Refactoring. Merge this page with VocabularyPage
export default class ExpressionsPage extends Component {
	constructor(props) {
		super(props);

		this.state = {
			selectedListId: 0
		};

		this.selectList = this.selectList.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	selectList(listId) {
		this.setState({ selectedListId: listId });
	}

	handleSubmit(event) {
		event.preventDefault();

		const value = this.refs.input.getValue();

		if (value.length === 0) {
			return;
		}

		Meteor.call('expressions.insert', value, this.state.selectedListId, (error) => {
			if (error) {
				Alerts.error(error.reason);
			} else {
				Alerts.success('Expressions added', 'Undo');
				this.refs.input.clear();
			}
		});
	}

	render() {
		// TODO: Put this in a component ?
		let lists = [{ id: 0, name: '— Select List —' }];

		this.props.lists.map((list) => {
			lists.push({ id: list._id, name: list.name });
		});

		return (
			<section className="section">
				<div className="container is-fluid">
					<div className="content">
						<h1 className="title">Expressions</h1>
					</div>

					<Select options={ lists } onChange={ this.selectList }/>

					{ !_.isEmpty(this.props.currentUser) && (parseInt(this.state.selectedListId) !== 0) ?
						<div>
							<hr />

							<form onSubmit={ this.handleSubmit } className="entry-form">
								<div className="control is-grouped">
									<Input type="text" label="Enter the expression and hit enter to create a new one" ref="input" />
									<Submit label="Add" />
								</div>
							</form>

							<ExpressionsList listId={ this.state.selectedListId } />
						</div> : ''
					}
				</div>
			</section>
		);
	}
}

ExpressionsPage.propTypes = {
	lists: PropTypes.array.isRequired,
	currentUser: PropTypes.object
};