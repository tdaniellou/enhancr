import React, { Component, PropTypes } from 'react';
import { createContainer } from 'meteor/react-meteor-data';

import { User } from '../../api/User.js';
import { Lists } from '../../api/lists/lists.js';

import VocabularyList from '../containers/VocabularyListContainer';

import Alerts from '../components/Alert/Alerts';

import Input from '../components/Form/Input';
import Select from '../components/Form/Select';
import Submit from '../components/Form/Submit';

// TODO: Refactoring. Merge this page with ExpressionsPage
export default class VocabularyPage extends Component {
	constructor(props) {
		super(props);

		this.state = {
			selectedListId: 0
		};

		this.selectList = this.selectList.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	selectList(listId) {
		this.setState({ selectedListId: listId });
	}

	handleSubmit(event) {
		event.preventDefault();

		const value = this.refs.input.getValue();

		if (value.length === 0) {
			return;
		}

		Meteor.call('vocabularies.insert', value, this.state.selectedListId, (error) => {
			if (error) {
				Alerts.error(error.reason);
			} else {
				Alerts.success('Vocabulary added', 'Undo');
				this.refs.input.clear();
			}
		});
	}

	render() {
		// Set the default list with an id of 0.
		// This way, we just have to check whether the selected id is 0 or not in order to show the table of vocabulary
		let lists = [{ id: 0, name: '— Select List —' }];

		this.props.lists.map((list) => {
			lists.push({ id: list._id, name: list.name });
		});

		return (
			<section className="section">
				<div className="container is-fluid">
					<div className="content">
						<h1 className="title">Vocabulary</h1>
					</div>

					<Select options={ lists } onChange={ this.selectList }/>

					{ !_.isEmpty(this.props.currentUser) && (parseInt(this.state.selectedListId) !== 0) ?
						<div>
							<hr/>

							<form onSubmit={ this.handleSubmit } className="entry-form">
								<div className="control is-grouped">
									<Input type="text" label="Enter the vocabulary and hit enter to create a new one" ref="input" />
									<Submit label="Add" />
								</div>
							</form>

							<VocabularyList listId={ this.state.selectedListId } />
						</div> : ''
					}
				</div>
			</section>
		);
	}
}

VocabularyPage.propTypes = {
	lists: PropTypes.array.isRequired,
	currentUser: PropTypes.object
};