import React, { Component } from 'react';
import { Link, browserHistory } from 'react-router';

import { User } from '../../api/User';

import Notification from '../components/Notification';

import Alerts from '../components/Alert/Alerts';

import Input from '../components/Form/Input';
import Submit from '../components/Form/Submit';

class RegisterPage extends Component {
	constructor(props) {
		super(props);

		this.state = {
			errors: {}
		};

		this.onSubmit = this.onSubmit.bind(this);
	}

	onSubmit(event) {
		event.preventDefault();

		// Reset the errors in the state
		this.setState({ errors: {} });

		// Get the form input DOMNodes
		const username = this.refs.username;
		const email = this.refs.email;
		const password = this.refs.password
		const password2 = this.refs.password2;

		const usernameValue = username.getValue();
		const emailValue = email.getValue();
		const passwordValue = password.getValue();
		const password2Value = password2.getValue();

		// The list of errors
		const errors = {};

		if (!usernameValue) {
			errors.username = 'Username is required';
		}

		if (!emailValue) {
			errors.email = 'Email is required';
		}

		if (!passwordValue) {
			errors.password = 'Password is required';
		}

		if (passwordValue !== password2Value) {
			errors.password2 = 'Passwords do not match';
		}

		if (!_.isEmpty(errors)) {
			this.setState({ errors: errors });

			return;
		}

		User.create({
			username: usernameValue,
			email: emailValue,
			password: passwordValue,
		}, (error) => {
			if (error) {
				errors.error = error.reason;

				this.setState({ errors: errors });
			} else {
				// Clear the form
				username.clear();
				email.clear();
				password.clear();
				password2.clear();

				// Redirect
				browserHistory.push('/');
			}
		});
	}

	render() {
		const formErrors = Object.keys(this.state.errors).map((key) => {
			return <Notification key={ key } content={ this.state.errors[key] } color="is-danger" />;
		});

		return (
			<section className="section">
				<div className="container">
					<div className="columns">
						<div className="column is-one-third is-offset-one-third">
							<form onSubmit={ this.onSubmit } className="auth-form">
								<h1 className="title">Register</h1>
								{ formErrors }
								<div>
									<Input ref="username" type="text" label="Username" icon="fa fa-user" autofocus={ true }/>
									<Input ref="email" type="email" label="Email" icon="fa fa-envelope" />
									<Input ref="password" type="password" label="Password" icon="fa fa-lock" />
									<Input ref="password2" type="password" label="Confirm Password" icon="fa fa-lock" />
									<Submit label="Submit" />
								</div>
							</form>
							<p className="auth-alt">Have an account? <Link to="/login">Sign-in</Link></p>
						</div>
					</div>
				</div>
			</section>
		);
	}
}

export default RegisterPage;