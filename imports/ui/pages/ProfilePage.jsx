import React, { Component, PropTypes } from 'react';
import { createContainer } from 'meteor/react-meteor-data';

import { User } from '../../api/User.js';

import Hero from '../components/Hero/Hero';
import HeroBody from '../components/Hero/HeroBody';

class ProfilePage extends Component {
	render() {
		const { currentUser } = this.props;

		return (
			<div>
				<Hero size="medium" color="is-dark">
					<HeroBody>
						<div className="columns">
							<div className="column is-2">
								<div className="avatar image is-128x128">
									<img src="http://placehold.it/256x256" className="is-circular"/>
								</div>
							</div>
							<div className="column">
								<p className="title is-2">{ currentUser.username }</p>
								<p className="subtitle is-4">Description</p>
							</div>
						</div>
					</HeroBody>
				</Hero>
				<section className="section">
					<div className="container is-fluid">
						<div className="tabs is-toggle is-centered">
							<ul>
								<li className="is-active">
									<a>0 lists</a>
								</li>
								<li>
									<a>0 starred</a>
								</li>
							</ul>
						</div>
						<div className="box">
							<p className="title is-4">No lists</p>
							<p className="subtitle is-6">Come back later ;)</p>
						</div>
					</div>
				</section>
			</div>
		);
	}
}

ProfilePage.propTypes = {
	currentUser: PropTypes.object.isRequired
}

export default createContainer(() => {
	return {
		currentUser: User.get()
	}
}, ProfilePage);