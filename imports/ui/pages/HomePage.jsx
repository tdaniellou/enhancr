import React, { Component, PropTypes } from 'react';
import { createContainer } from 'meteor/react-meteor-data';

import Card from '../components/Card';
import Icon from '../components/Icon';

import Hero from '../components/Hero/Hero';
import HeroBody from '../components/Hero/HeroBody';
import HeroFooter from '../components/Hero/HeroFooter';

import Grid from '../components/Grid/Grid';

import Input from '../components/Form/Input';

class HomePage extends Component {
	constructor(props) {
		super(props);
	}

	renderCards(cards) {
		let renderedCards = [];

		cards.map((card, i) => {
			renderedCards.push(
				<Card key={ card } title="Title"
					subtitle="Subtitle"
					description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec iaculis mauris."/>
			);
		});

		return renderedCards;
	}

	render() {
		const cards = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

		return (
			<div>
				<Hero size="fullheight" color="is-black">
					<HeroBody align="center">
						<h1 className="title">
							Explore lists of language
						</h1>
						<h2 className="subtitle">
							Subtitle
						</h2>
						<div className="columns">
							<div className="column is-half is-offset-one-quarter">
								<div className="control is-grouped">
									<div className="control is-expanded">
										<Input type="text" label="Search for a list..." icon="fa fa-search" autofocus={ true }/>
									</div>
									<div className="control">
										<a className="button is-danger">
											Search
										</a>
									</div>
								</div>
							</div>
						</div>
					</HeroBody>
					<HeroFooter align="center">
						<Icon icon="arrow-down" size="large" />
					</HeroFooter>
				</Hero>
				<section className="section">
					<div className="container">
						<div className="content">
							<h1 className="title">Home</h1>
						</div>

						<div className="tabs is-centered">
							<ul>
								<li className="is-active"><a>Recent</a></li>
								<li><a>Trending</a></li>
								<li><a>Most used</a></li>
								<li><a>Top searches</a></li>
							</ul>
						</div>

						<Grid cols={ 4 }>
							{ this.renderCards(cards) }
						</Grid>
					</div>
				</section>
			</div>
		)
	}
}

HomePage.propTypes = {
};

export default createContainer(() => {
	return {};
}, HomePage);