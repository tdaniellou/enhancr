import React, { Component, PropTypes } from 'react';
import { createContainer } from 'meteor/react-meteor-data';

import { User } from '../../api/User.js';
import { Lists } from '../../api/lists/lists.js';

import List from '../components/List/List';
import ListDetail from '../components/List/ListDetail';

import Notification from '../components/Notification';

import Modal from '../components/Modal';

import Alerts from '../components/Alert/Alerts';

import Input from '../components/Form/Input';
import Submit from '../components/Form/Submit';

class DashboardPage extends Component {
	constructor(props) {
		super(props);

		this.state = {
			isModalOpen: false,
			selectedList: null
		};

		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleUpdate = this.handleUpdate.bind(this);
		this.handleDelete = this.handleDelete.bind(this);

		this.renderModalContent = this.renderModalContent.bind(this);
		this.renderLists = this.renderLists.bind(this);

		this.openModal = this.openModal.bind(this);
		this.closeModal = this.closeModal.bind(this);
		this.toggleModal = this.toggleModal.bind(this);
		this.selectList = this.selectList.bind(this);
	}

	handleSubmit(event) {
		event.preventDefault();

		const listName = this.refs.input.getValue();

		if (listName.length === 0) {
			return;
		}

		Meteor.call('lists.insert', listName, (error) => {
			if (error) {
				Alerts.error(error.reason);
			} else {
				Alerts.success('List created', 'Undo');
				this.refs.input.clear();
			}
		});
	}

	handleDelete(listId) {
		Meteor.call('lists.remove', listId, (error) => {
			if (error) {
				Alerts.error(error.reason);
			} else {
				Alerts.success('List deleted', 'Undo');
				this.closeModal();
			}
		});
	}

	handleUpdate(newValues, callback) {
		const listId = newValues._id;

		Meteor.call('lists.update', listId, newValues, (error) => {
			if (error) {
				Alerts.error(error.reason);
			} else {
				Alerts.success('List updated', 'Undo');

				if (callback) {
					callback();
				}
			}
		});
	}

	openModal() {
		this.setState({ isModalOpen: true });
	}

	closeModal() {
		this.setState({ isModalOpen: false });
	}

	toggleModal() {
		this.setState({ isModalOpen: !this.state.isModalOpen });
	}

	selectList(list) {
		// Store the list
		this.setState({ selectList: list });

		// Open the modal
		this.toggleModal();
	}

	renderModalContent() {
		if (!this.state.isModalOpen) {
			return;
		}

		return <ListDetail list={ this.state.selectList } onDelete={ this.handleDelete } onUpdate={ this.handleUpdate }/>;
	}

	renderLists() {
		return this.props.lists.map((list) => {
			return <List key={ list._id } list={ list } onClick={ this.selectList.bind(null, list) }/>
		});
	}

	render() {
		return (
			<section className="section">
				<div className="container is-fluid">
					<div className="content">

						{ !_.isEmpty(this.props.currentUser) ?
							<div>
								<h1 className="title">Dashboard</h1>

								<h2 className="title is-4">Your lists</h2>

								<form onSubmit={ this.handleSubmit } className="entry-form">
									<div className="control is-grouped">
										<Input ref="input" type="text" label="Enter the name list and hit enter to create a new one" className="is-expanded"/>
										<Submit label="Create" />
									</div>
								</form>

								<ul>
									{ this.renderLists() }
								</ul>

								<Modal isOpen={ this.state.isModalOpen } onClose={ this.toggleModal }>
									{ this.renderModalContent() }
								</Modal>
							</div>
							: ''
						}

					</div>
				</div>
			</section>
		)
	}
}

DashboardPage.propTypes = {
	lists: PropTypes.array.isRequired,
	currentUser: PropTypes.object
};

export default DashboardPage;